﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.ViewModels.BankEquipments;

namespace TechPoint.DesktopApp.ViewModels
{
    class MainViewModel : Conductor<ITab>.Collection.OneActive, IShell
    {
        private INavigationService navigationService;
        private readonly SimpleContainer container;

        public MainViewModel(SimpleContainer container, IEnumerable<ITab> tabItems, INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.container = container;
            container.Instance(navigationService);

            DisplayName = "TechPoint";
            Items.AddRange(tabItems);
        }
    }
}
