﻿
using Caliburn.Micro;
using System;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.References.BankEquipmentAccessories
{
    public class BankEquipmentAccessoryViewModel : ReferenceTabViewModelBase
    {
        private readonly IRepository<BankEquipmentAccessory> bankEquipmentAccessoryRepository;
        private readonly IRepository<AccessoryType> accessoryTypeRepository;
        private const String ROOT_DIALOG = "RootBankEquipmentAccessoryDialog";

        public BankEquipmentAccessoryViewModel(IRepository<BankEquipmentAccessory> bankEquipmentAccessoryRepository,
            IRepository<AccessoryType> accessoryTypeRepository)
        {
            this.bankEquipmentAccessoryRepository = bankEquipmentAccessoryRepository;
            this.accessoryTypeRepository = accessoryTypeRepository;

            DisplayName = "Комплектующие БО";

            BankEquipmentAccessories = new BindableCollection<BankEquipmentAccessory>(bankEquipmentAccessoryRepository.GetAll());
            AccessoryTypes = new BindableCollection<AccessoryType>(accessoryTypeRepository.GetAll());

            SelectedBankEquipmentAccessory = new BankEquipmentAccessory();
        }

        #region Enities Properties

        private BindableCollection<BankEquipmentAccessory> bankEquipmentAccessories;

        public BindableCollection<BankEquipmentAccessory> BankEquipmentAccessories
        {
            get { return bankEquipmentAccessories; }
            set
            {
                bankEquipmentAccessories = value;
                NotifyOfPropertyChange(() => BankEquipmentAccessories);
            }
        }

        private BankEquipmentAccessory selectedBankEquipmentAccessory;

        public BankEquipmentAccessory SelectedBankEquipmentAccessory
        {
            get { return selectedBankEquipmentAccessory; }
            set
            {
                selectedBankEquipmentAccessory = value;
                NotifyOfPropertyChange(() => SelectedBankEquipmentAccessory);
            }
        }

        private BindableCollection<AccessoryType> accessoryTypes;

        public BindableCollection<AccessoryType> AccessoryTypes
        {
            get { return accessoryTypes; }
            set
            {
                accessoryTypes = value;
                NotifyOfPropertyChange(() => AccessoryTypes);
            }
        }


        private AccessoryType selectedAccessoryType;

        public AccessoryType SelectedAccessoryType
        {
            get { return selectedAccessoryType; }
            set
            {
                selectedAccessoryType = value;
                NotifyOfPropertyChange(() => SelectedAccessoryType);
            }
        }

        #endregion

        public void Add()
        {
            SelectedBankEquipmentAccessory = new BankEquipmentAccessory();
        }

        public void Save()
        {
            if (SelectedBankEquipmentAccessory != null)
            {
                if (SelectedBankEquipmentAccessory.BankEquipmentAccessoryId == 0)
                {
                    UpdateValues();
                    bankEquipmentAccessoryRepository.Add(SelectedBankEquipmentAccessory);
                    BankEquipmentAccessories = new BindableCollection<BankEquipmentAccessory>(bankEquipmentAccessoryRepository.GetAll());
                }
                else if (SelectedBankEquipmentAccessory.BankEquipmentAccessoryId != 0)
                {
                    UpdateValues();
                    bankEquipmentAccessoryRepository.Update(SelectedBankEquipmentAccessory);
                    BankEquipmentAccessories = new BindableCollection<BankEquipmentAccessory>(bankEquipmentAccessoryRepository.GetAll());
                }
            }
        }

        private void UpdateValues()
        {
            if (SelectedAccessoryType != null)
            {
                SelectedBankEquipmentAccessory.AccessoryTypeId = SelectedAccessoryType.AccessoryTypeId;
            }
        }

        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (SelectedBankEquipmentAccessory != null)
            {
                try
                {
                    bankEquipmentAccessoryRepository.Delete(SelectedBankEquipmentAccessory);
                    BankEquipmentAccessories.Remove(SelectedBankEquipmentAccessory);
                    SelectedBankEquipmentAccessory = new BankEquipmentAccessory();
                    return true;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
    }
}
