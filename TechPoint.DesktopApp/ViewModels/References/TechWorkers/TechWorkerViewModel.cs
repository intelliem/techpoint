﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.References.TechWorkers
{
    public class TechWorkerViewModel : ReferenceTabViewModelBase
    {
        private readonly IRepository<TechWorker> techWorkerRepository;
        private readonly IRepository<Post> postRepository;
        private const String ROOT_DIALOG = "RootTechWorkerDialog";

        public TechWorkerViewModel(IRepository<TechWorker> techWorkerRepository, IRepository<Post> postRepository)
        {
            this.techWorkerRepository = techWorkerRepository;
            this.postRepository = postRepository;

            DisplayName = "Сотрудники";

            TechWorkers = new BindableCollection<TechWorker>(techWorkerRepository.GetAll());
            Posts = new BindableCollection<Post>(postRepository.GetAll());

            SelectedTechWorker = new TechWorker();
        }

        #region Enities Properties

        private BindableCollection<TechWorker> techWorkers;

        public BindableCollection<TechWorker> TechWorkers
        {
            get { return techWorkers; }
            set
            {
                techWorkers = value;
                NotifyOfPropertyChange(() => TechWorkers);
            }
        }

        private TechWorker selectedTechWorker;

        public TechWorker SelectedTechWorker
        {
            get { return selectedTechWorker; }
            set
            {
                selectedTechWorker = value;
                NotifyOfPropertyChange(() => SelectedTechWorker);
            }
        }

        private BindableCollection<Post> posts;

        public BindableCollection<Post> Posts
        {
            get { return posts; }
            set
            {
                posts = value;
                NotifyOfPropertyChange(() => Posts);
            }
        }

        private Post selectedPost;

        public Post SelectedPost
        {
            get { return selectedPost; }
            set
            {
                selectedPost = value;
                NotifyOfPropertyChange(() => Posts);
            }
        }

        #endregion


        public void Add()
        {
            SelectedTechWorker = new TechWorker();
        }

        public void Save()
        {
            if (SelectedTechWorker != null)
            {
                if (SelectedTechWorker.TechWorkerId == 0)
                {
                    UpdateValues();
                    techWorkerRepository.Add(SelectedTechWorker);
                    TechWorkers = new BindableCollection<TechWorker>(techWorkerRepository.GetAll());
                }
                else if (SelectedTechWorker.TechWorkerId != 0)
                {
                    UpdateValues();
                    techWorkerRepository.Update(SelectedTechWorker);
                    TechWorkers = new BindableCollection<TechWorker>(techWorkerRepository.GetAll());
                }
            }
        }

        private void UpdateValues()
        {
            if (SelectedTechWorker != null)
            {
                SelectedTechWorker.PostId = SelectedPost.PostId;
            }
        }

        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (SelectedTechWorker != null)
            {
                try
                {
                    techWorkerRepository.Delete(SelectedTechWorker);
                    TechWorkers.Remove(SelectedTechWorker);
                    SelectedTechWorker = new TechWorker();
                    return true;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
    }
}
