﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.References
{
    public class ReferencesViewModel : Conductor<IReferenceTab>.Collection.OneActive, ITab
    {
        public ReferencesViewModel(IEnumerable<IReferenceTab> referenceTabItems)
        {
            DisplayName = "Справочники";
            Items.AddRange(referenceTabItems);
        }
    }
}
