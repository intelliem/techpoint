﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.References.BankEquipmentTypes
{
    public class BankEquipmentTypeViewModel : ReferenceTabViewModelBase
    {
        private readonly IRepository<BankEquipmentType> bankEquipmentTypeRepository;
        private const String ROOT_DIALOG = "RootBankEquipmentTypeDialog";

        public BankEquipmentTypeViewModel(IRepository<BankEquipmentType> bankEquipmentTypeRepository)
        {
            DisplayName = "Типы БО";
            this.bankEquipmentTypeRepository = bankEquipmentTypeRepository;

            SelectedBankEquipmentType = new BankEquipmentType();
            BankEquipmentTypes = new BindableCollection<BankEquipmentType>(bankEquipmentTypeRepository.GetAll());
        }



        #region Enities Properties

        private BindableCollection<BankEquipmentType> bankEquipmentTypes;

        public BindableCollection<BankEquipmentType> BankEquipmentTypes
        {
            get { return bankEquipmentTypes; }
            set
            {
                bankEquipmentTypes = value;
                NotifyOfPropertyChange(() => BankEquipmentTypes);
            }
        }

        private BankEquipmentType selectedBankEquipmentType;

        public BankEquipmentType SelectedBankEquipmentType
        {
            get { return selectedBankEquipmentType; }
            set
            {
                selectedBankEquipmentType = value;
                NotifyOfPropertyChange(() => SelectedBankEquipmentType);
            }
        }

        #endregion


        public void Add()
        {
            SelectedBankEquipmentType = new BankEquipmentType();
        }

        public void Save()
        {
            if (SelectedBankEquipmentType != null)
            {
                if (SelectedBankEquipmentType.BankEquipmentTypeId == 0)
                {
                    bankEquipmentTypeRepository.Add(SelectedBankEquipmentType);
                    BankEquipmentTypes = new BindableCollection<BankEquipmentType>(bankEquipmentTypeRepository.GetAll());
                }
                else if (SelectedBankEquipmentType.BankEquipmentTypeId != 0)
                {
                    bankEquipmentTypeRepository.Update(SelectedBankEquipmentType);
                }
            }
        }


        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (SelectedBankEquipmentType != null)
            {
                try
                {
                    bankEquipmentTypeRepository.Delete(SelectedBankEquipmentType);
                    BankEquipmentTypes.Remove(SelectedBankEquipmentType);
                    SelectedBankEquipmentType = new BankEquipmentType();
                    return true;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
    }
}
