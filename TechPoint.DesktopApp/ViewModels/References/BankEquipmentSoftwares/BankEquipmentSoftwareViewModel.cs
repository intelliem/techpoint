﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.References.BankEquipmentSoftwares
{
    public class BankEquipmentSoftwareViewModel : ReferenceTabViewModelBase
    {
        private readonly IRepository<BankEquipmentSoftware> bankEquipmentSoftwareRepository;
        private readonly IRepository<SoftwareType> softwareTypeRepository;
        private const String ROOT_DIALOG = "RootBankEquipmentSoftwareDialog";

        public BankEquipmentSoftwareViewModel(IRepository<BankEquipmentSoftware> bankEquipmentSoftwareRepository,
            IRepository<SoftwareType> softwareTypeRepository)
        {
            DisplayName = "Программное обеспечение БО";

            this.bankEquipmentSoftwareRepository = bankEquipmentSoftwareRepository;
            this.softwareTypeRepository = softwareTypeRepository;

            BankEquipmentSoftwares = new BindableCollection<BankEquipmentSoftware>(bankEquipmentSoftwareRepository.GetAll());
            SoftwareTypes = new BindableCollection<SoftwareType>(softwareTypeRepository.GetAll());

            
        }

        #region Enities Properties

        private BindableCollection<BankEquipmentSoftware> bankEquipmentSoftwares;

        public BindableCollection<BankEquipmentSoftware> BankEquipmentSoftwares
        {
            get { return bankEquipmentSoftwares; }
            set
            {
                bankEquipmentSoftwares = value;
                NotifyOfPropertyChange(() => BankEquipmentSoftwares);
            }
        }

        private BankEquipmentSoftware selectedBankEquipmentSoftware;

        public BankEquipmentSoftware SelectedBankEquipmentSoftware
        {
            get { return selectedBankEquipmentSoftware; }
            set
            {
                selectedBankEquipmentSoftware = value;
                NotifyOfPropertyChange(() => SelectedBankEquipmentSoftware);
            }
        }

        private BindableCollection<SoftwareType> softwareTypes;

        public BindableCollection<SoftwareType> SoftwareTypes
        {
            get { return softwareTypes; }
            set { softwareTypes = value; }
        }

        private SoftwareType selectedSoftwareType;

        public SoftwareType SelectedSoftwareType
        {
            get { return selectedSoftwareType; }
            set
            {
                selectedSoftwareType = value;
                NotifyOfPropertyChange(() => SelectedSoftwareType);
            }
        }



        #endregion

        public void Add()
        {
            SelectedBankEquipmentSoftware = new BankEquipmentSoftware();
        }

        public void Save()
        {
            if (SelectedBankEquipmentSoftware != null)
            {
                if (SelectedBankEquipmentSoftware.BankEquipmentSoftwareId == 0)
                {
                    UpdateValues();
                    bankEquipmentSoftwareRepository.Add(SelectedBankEquipmentSoftware);
                    BankEquipmentSoftwares = new BindableCollection<BankEquipmentSoftware>(bankEquipmentSoftwareRepository.GetAll());
                }
                else if (SelectedBankEquipmentSoftware.BankEquipmentSoftwareId != 0)
                {
                    UpdateValues();
                    bankEquipmentSoftwareRepository.Update(SelectedBankEquipmentSoftware);
                    BankEquipmentSoftwares = new BindableCollection<BankEquipmentSoftware>(bankEquipmentSoftwareRepository.GetAll());
                }
            }
        }

        private void UpdateValues()
        {
            if (SelectedSoftwareType != null)
            {
                SelectedBankEquipmentSoftware.SoftwareTypeId = SelectedSoftwareType.SoftwareTypeId;
            }
        }

        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (SelectedBankEquipmentSoftware != null)
            {
                try
                {
                    bankEquipmentSoftwareRepository.Delete(SelectedBankEquipmentSoftware);
                    BankEquipmentSoftwares.Remove(SelectedBankEquipmentSoftware);
                    SelectedBankEquipmentSoftware = new BankEquipmentSoftware();
                    return true;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
    }
}
