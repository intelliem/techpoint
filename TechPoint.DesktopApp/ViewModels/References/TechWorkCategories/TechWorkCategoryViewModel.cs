﻿using Caliburn.Micro;
using System;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.References.TechWorkCategories
{
    public class TechWorkCategoryViewModel : ReferenceTabViewModelBase
    {
        private readonly IRepository<TechWorkCategory> techWorkCategoryRepository;
        private const String ROOT_DIALOG = "RootTechWorkerCategoryDialog";

        public TechWorkCategoryViewModel(IRepository<TechWorkCategory> techWorkCategoryRepository)
        {
            this.techWorkCategoryRepository = techWorkCategoryRepository;

            DisplayName = "Виды работ";

            TechWorkCategories = new BindableCollection<TechWorkCategory>(techWorkCategoryRepository.GetAll());
            SelectedTechWorkCategory = new TechWorkCategory();
        }

        #region Enities Properties

        private BindableCollection<TechWorkCategory> techWorkCategories;

        public BindableCollection<TechWorkCategory> TechWorkCategories
        {
            get { return techWorkCategories; }
            set
            {
                techWorkCategories = value;
                NotifyOfPropertyChange(() => TechWorkCategories);
            }
        }

        private TechWorkCategory selectedTechWorkCategory;

        public TechWorkCategory SelectedTechWorkCategory
        {
            get { return selectedTechWorkCategory; }
            set
            {
                selectedTechWorkCategory = value;
                NotifyOfPropertyChange(() => SelectedTechWorkCategory);
            }
        }

        #endregion

        public void Add()
        {
            SelectedTechWorkCategory = new TechWorkCategory();
        }

        public void Save()
        {
            if (SelectedTechWorkCategory != null)
            {
                if (SelectedTechWorkCategory.TechWorkCategoryId == 0)
                {
                    techWorkCategoryRepository.Add(SelectedTechWorkCategory);
                    TechWorkCategories = new BindableCollection<TechWorkCategory>(techWorkCategoryRepository.GetAll());
                }
                else if (SelectedTechWorkCategory.TechWorkCategoryId != 0)
                {
                    techWorkCategoryRepository.Update(SelectedTechWorkCategory);
                    TechWorkCategories = new BindableCollection<TechWorkCategory>(techWorkCategoryRepository.GetAll());
                }
            }
        }

        private void UpdateValues()
        {
            
        }

        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (SelectedTechWorkCategory != null)
            {
                try
                {
                    techWorkCategoryRepository.Delete(SelectedTechWorkCategory);
                    TechWorkCategories.Remove(SelectedTechWorkCategory);
                    SelectedTechWorkCategory = new TechWorkCategory();
                    return true;
                }
                catch (Exception e)
                {
                }
            }
            return false;
        }
    }
}
