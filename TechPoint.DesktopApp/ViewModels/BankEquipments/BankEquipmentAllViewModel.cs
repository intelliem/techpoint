﻿using Caliburn.Micro;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DataService.Repositories;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.BankEquipments
{
    class BankEquipmentAllViewModel : CrudViewModelBase
    {
        private readonly IRepository<BankEquipment> bankEquipmentRepository;
        private readonly IEventAggregator eventAggregator;
        private const String ROOT_DIALOG = "RootBankEquipmentDialog";

        public BankEquipmentAllViewModel(IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<BankEquipmentType> bankEquipmentTypeRepository,
            IRepository<Manufacturer> manufactureRepository,
            IRepository<ServiceAddress> serviceAddressRepository,
            IEventAggregator eventAggregator)
        {
            this.bankEquipmentRepository = bankEquipmentRepository;
            this.eventAggregator = eventAggregator;

            BankEquipments = new BindableCollection<BankEquipment>(bankEquipmentRepository.GetAll());
        }

        private BindableCollection<BankEquipment> bankEquipments;
        public BindableCollection<BankEquipment> BankEquipments
        {
            get { return bankEquipments; }
            set
            {
                if (bankEquipments == value)
                {
                    return;
                }

                bankEquipments = value;
                NotifyOfPropertyChange(() => BankEquipments);
            }
        }

        private BankEquipment bankEquipment;
        public BankEquipment BankEquipment
        {
            get { return bankEquipment; }
            set
            {
                bankEquipment = value;
                NotifyOfPropertyChange(() => BankEquipment);
            }
        }

        public void Edit()
        {
            eventAggregator.PublishOnUIThread(BankEquipment);
        }

        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (BankEquipment != null)
            {
                try
                {
                    bankEquipmentRepository.Delete(BankEquipment);
                    BankEquipments.Remove(BankEquipment);
                    BankEquipment = null;
                    return true;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
    }
}
