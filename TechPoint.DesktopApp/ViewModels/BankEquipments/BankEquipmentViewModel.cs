﻿using Caliburn.Micro;
using System.Collections.Generic;
using System.Net.Mime;
using System.Windows.Controls;
using System.Windows.Navigation;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DataService.Repositories;
using TechPoint.DesktopApp.Infrastructure;

namespace TechPoint.DesktopApp.ViewModels.BankEquipments
{
    public class BankEquipmentViewModel : Conductor<IForm>.Collection.OneActive, ITab, IHandle<string>, IHandle<BankEquipment>
    {
        private readonly IRepository<BankEquipment> bankEquipmentRepository;
        private readonly IRepository<BankEquipmentType> bankEquipmentTypeRepository;
        private readonly IRepository<Manufacturer> manufactureRepository;
        private readonly IRepository<ServiceAddress> serviceAddressRepository;

        private INavigationService navigationService;
        private readonly SimpleContainer container;
        private readonly IEventAggregator eventAggregator;

        public BankEquipmentViewModel(SimpleContainer container, 
            IEventAggregator eventAggregator,
            INavigationService navigationService, 
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<BankEquipmentType> bankEquipmentTypeRepository,
            IRepository<Manufacturer> manufactureRepository,
            IRepository<ServiceAddress> serviceAddressRepository)
        {
            this.container = container;
            this.bankEquipmentRepository = bankEquipmentRepository;
            this.bankEquipmentTypeRepository = bankEquipmentTypeRepository;
            this.manufactureRepository = manufactureRepository;
            this.serviceAddressRepository = serviceAddressRepository;

            this.navigationService = navigationService;
            this.eventAggregator = eventAggregator;

            eventAggregator.Subscribe(this);

            ActivateItem(new BankEquipmentAllViewModel(bankEquipmentRepository,
                 bankEquipmentTypeRepository,
                manufactureRepository,
                serviceAddressRepository,
                eventAggregator));
            DisplayName = "Банковское оборудование";
        }

        public void BankEquipmentAddNavigator()
        {
            ActivateItem(new BankEquipmentAddViewModel(eventAggregator,
                bankEquipmentRepository,
                bankEquipmentTypeRepository,
                manufactureRepository,
                serviceAddressRepository
                ));
        }

        public void Handle(string message)
        {
            if (message.Equals("Cancel"))
            {
                ActivateItem(new BankEquipmentAllViewModel(bankEquipmentRepository,
                    bankEquipmentTypeRepository,
                    manufactureRepository,
                    serviceAddressRepository,
                    eventAggregator));
            }

            if (message.Equals("Save"))
            {
                ActivateItem(new BankEquipmentAllViewModel(bankEquipmentRepository,
                    bankEquipmentTypeRepository,
                    manufactureRepository,
                    serviceAddressRepository,
                    eventAggregator));
            }
        }

        public void Handle(BankEquipment item)
        {
            ActivateItem(new BankEquipmentAddViewModel(eventAggregator,
                bankEquipmentRepository,
                bankEquipmentTypeRepository,
                manufactureRepository,
                serviceAddressRepository,
                item));
        }
    }
}
