﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using TechPoint.DataService.Adapter;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DataService.Repositories;
using TechPoint.DesktopApp.Infrastructure;
using Action = System.Action;
using Screen = Caliburn.Micro.Screen;

namespace TechPoint.DesktopApp.ViewModels.BankEquipments
{
    public class BankEquipmentAddViewModel : CrudViewModelBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRepository<BankEquipment> bankEquipmentRepository;
        private readonly IRepository<BankEquipmentType> bankEquipmentTypeRepository;
        private readonly IRepository<Manufacturer> manufactureRepository;
        private readonly IRepository<ServiceAddress> serviceAddressRepository;

        public BankEquipmentAddViewModel(IEventAggregator eventAggregator,
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<BankEquipmentType> bankEquipmentTypeRepository,
            IRepository<Manufacturer> manufactureRepository,
            IRepository<ServiceAddress> serviceAddressRepository)
        {
            this.eventAggregator = eventAggregator;
            this.bankEquipmentRepository = bankEquipmentRepository;
            this.bankEquipmentTypeRepository = bankEquipmentTypeRepository;
            this.manufactureRepository = manufactureRepository;
            this.serviceAddressRepository = serviceAddressRepository;

            NewBankEquipment = new BankEquipment();
            BankEquipmentTypes = new BindableCollection<BankEquipmentType>(bankEquipmentTypeRepository.GetAll());
            ServiceAddresses = new BindableCollection<ServiceAddressResponse>(serviceAddressRepository.GetAll()
                .Select(i =>
                {
                    string address = i.City + ", " + i.Street + ", " + i.House;
                    return new ServiceAddressResponse(i.ServiceAddressId, address);
                }).ToList());
            Manufacturers = new BindableCollection<Manufacturer>(manufactureRepository.GetAll());
        }

        public BankEquipmentAddViewModel(IEventAggregator eventAggregator,
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<BankEquipmentType> bankEquipmentTypeRepository,
            IRepository<Manufacturer> manufactureRepository,
            IRepository<ServiceAddress> serviceAddressRepository,
            BankEquipment bankEquipment)
        {
            this.eventAggregator = eventAggregator;
            this.bankEquipmentRepository = bankEquipmentRepository;

            BankEquipmentTypes = new BindableCollection<BankEquipmentType>(bankEquipmentTypeRepository.GetAll());
            ServiceAddresses = new BindableCollection<ServiceAddressResponse>(serviceAddressRepository.GetAll()
                .Select(i =>
                {
                    string address = i.City + ", " + i.Street + ", " + i.House;
                    return new ServiceAddressResponse(i.ServiceAddressId, address);
                }).ToList());
            Manufacturers = new BindableCollection<Manufacturer>(manufactureRepository.GetAll());

            NewBankEquipment = bankEquipment;
            SelectedEquipmentType = bankEquipment.BankEquipmentType;
            SelectedServiceAddress = new ServiceAddressResponse(bankEquipment.ServiceAddress);
        }


        #region Entity Properties

        private BankEquipmentType selectedEquipmentType;

        public BankEquipmentType SelectedEquipmentType
        {
            get { return selectedEquipmentType; }
            set
            {
                selectedEquipmentType = value;
                NotifyOfPropertyChange(() => SelectedEquipmentType);
            }
        }


        private BindableCollection<BankEquipmentType> bankEquipmentTypes;

        public BindableCollection<BankEquipmentType> BankEquipmentTypes
        {
            get { return bankEquipmentTypes; }
            set
            {
                bankEquipmentTypes = value;
                NotifyOfPropertyChange(() => BankEquipmentTypes);
            }
        }

        private Manufacturer selectedManufacturer;

        public Manufacturer SelectedManufacturer
        {
            get { return selectedManufacturer; }
            set
            {
                selectedManufacturer = value;
                NotifyOfPropertyChange(() => SelectedManufacturer);
            }
        }


        private BindableCollection<Manufacturer> manufacturers;

        public BindableCollection<Manufacturer> Manufacturers
        {
            get { return manufacturers; }
            set
            {
                manufacturers = value;
                NotifyOfPropertyChange(() => Manufacturers);
            }
        }

        private BindableCollection<ServiceAddressResponse> serviceAddresses;

        public BindableCollection<ServiceAddressResponse> ServiceAddresses
        {
            get { return serviceAddresses; }
            set
            {
                serviceAddresses = value;
                if (serviceAddresses == value)
                {
                    return;
                }

                NotifyOfPropertyChange(() => ServiceAddresses);
            }
        }

        private ServiceAddressResponse selectedServiceAddress;

        public ServiceAddressResponse SelectedServiceAddress
        {
            get { return selectedServiceAddress; }
            set
            {
                selectedServiceAddress = value;
                if (selectedServiceAddress == value)
                {
                    return;
                }

                selectedServiceAddress = value;
                NotifyOfPropertyChange(() => SelectedServiceAddress);
            }
        }

        #endregion

        private BankEquipment newBankEquipment;
        public BankEquipment NewBankEquipment
        {
            get { return newBankEquipment; }
            set
            {
                newBankEquipment = value;
                NotifyOfPropertyChange(() => NewBankEquipment);
            }
        }

        public void Save()
        {
            if (NewBankEquipment != null)
            {
                if (NewBankEquipment.BankEquipmentId == 0)
                {
                    UpdateValues();
                    bankEquipmentRepository.Add(NewBankEquipment);
                    eventAggregator.PublishOnUIThread("Save");
                }
                else if (NewBankEquipment.BankEquipmentId != 0)
                {
                    UpdateValues();
                    bankEquipmentRepository.Update(NewBankEquipment);
                    eventAggregator.PublishOnUIThread("Save");
                }
            }
        }

        private void UpdateValues()
        {
            if (SelectedServiceAddress != null)
            {
                NewBankEquipment.ServiceAddressId = SelectedServiceAddress.ServiceAddressId;
            }
            if (SelectedEquipmentType != null)
            {
                NewBankEquipment.BankEquipmentTypeId = SelectedEquipmentType.BankEquipmentTypeId;
            }
            if (selectedManufacturer != null)
            {
                NewBankEquipment.ManufacturerId = SelectedManufacturer.ManufacturerId;
            }
        }

        public void Cancel()
        {
            eventAggregator.PublishOnUIThread("Cancel");
        }
    }
}
