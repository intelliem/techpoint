﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;

namespace TechPoint.DesktopApp.ViewModels.Reports
{
    public class ReportViewModel : TabViewModelBase
    {
        private readonly IRepository<TechServiceJournal> techServiceJournalRepository;
        private readonly IRepository<TechRepairJournal> techRepairJournalRepository;
        private readonly IRepository<BankEquipment> bankEquipmentRepository;

        public ReportViewModel(IRepository<TechServiceJournal> techServiceJournalRepository,
             IRepository<TechRepairJournal> techRepairJournalRepository,
             IRepository<BankEquipment> bankEquipmentRepository,
             IRepository<ServiceAddress> serviceAddressRepository)
        {
            this.techServiceJournalRepository = techServiceJournalRepository;
            this.bankEquipmentRepository = bankEquipmentRepository;

            DisplayName = "Отчеты";

            TechServiceJournals = new BindableCollection<TechServiceJournal>(techServiceJournalRepository.GetAll());
            TechRepairJournals = new BindableCollection<TechRepairJournal>(techRepairJournalRepository.GetAll());
            ServiceAddresses = new BindableCollection<ServiceAddress>(serviceAddressRepository.GetAll());        

            techServiceJournalListView = new ListCollectionView(TechServiceJournals);
        }

        private BindableCollection<TechServiceJournal> techServiceJournals;

        public BindableCollection<TechServiceJournal> TechServiceJournals
        {
            get { return techServiceJournals; }
            set
            {
                techServiceJournals = value;
                NotifyOfPropertyChange(() => TechServiceJournals);
                techServiceJournalListView = new ListCollectionView(TechServiceJournals);
                NotifyOfPropertyChange(() => techServiceJournalListView);
            }
        }

        private ListCollectionView techServiceJournalListView;

        public ICollectionView TechServiceJournalListView
        {
            get { return techServiceJournalListView; }
        }

        private DateTime techServiceDate1 = DateTime.Today;

        public DateTime TechServiceDate1
        {
            get { return techServiceDate1; }
            set
            {
                techServiceDate1 = value;
                NotifyOfPropertyChange(() => techServiceDate1);

                TechServiceJournalListView.Filter = new Predicate<object>(s => ((TechServiceJournal)s).StartDate >= TechServiceDate1 && ((TechServiceJournal)s).StartDate <= TechServiceDate2);
            }
        }

        private DateTime techServiceDate2 = DateTime.Today;

        public DateTime TechServiceDate2
        {
            get { return techServiceDate2; }
            set
            {
                techServiceDate2 = value;
                NotifyOfPropertyChange(() => TechServiceDate2);

                TechServiceJournalListView.Filter = new Predicate<object>(s => ((TechServiceJournal)s).StartDate >= TechServiceDate1 && ((TechServiceJournal)s).StartDate <= TechServiceDate2);

            }
        }

        #region RepairJournal
        private BindableCollection<TechRepairJournal> techRepairJournals;

        public BindableCollection<TechRepairJournal> TechRepairJournals
        {
            get { return techRepairJournals; }
            set
            {
                techRepairJournals = value;
                NotifyOfPropertyChange(() => TechRepairJournals);
                techRepairJournalsListView = new ListCollectionView(TechRepairJournals);
                NotifyOfPropertyChange(() => techRepairJournalsListView);
            }
        }

        private ListCollectionView techRepairJournalsListView;

        public ICollectionView TechRepairJournalsListView
        {
            get { return techRepairJournalsListView; }
        }

        private DateTime techRepairDate1 = DateTime.Today;

        public DateTime TechRepairDate1
        {
            get { return techRepairDate1; }
            set
            {
                techRepairDate1 = value;
                NotifyOfPropertyChange(() => techRepairDate1);

                TechRepairJournalsListView.Filter = new Predicate<object>(s => ((TechRepairJournal)s).StartDate >= TechRepairDate1 && ((TechRepairJournal)s).StartDate <= TechRepairDate2);
            }
        }

        private DateTime techRepairDate2 = DateTime.Today;

        public DateTime TechRepairDate2
        {
            get { return techRepairDate2; }
            set
            {
                techRepairDate2 = value;
                NotifyOfPropertyChange(() => TechRepairDate2);

                TechRepairJournalsListView.Filter = new Predicate<object>(s => ((TechRepairJournal)s).StartDate >= TechRepairDate1 && ((TechRepairJournal)s).StartDate <= TechRepairDate2);

            }
        }

        #endregion

        #region ServiceAddress

        public BindableCollection<BankEquipment> BankEquipmentsFromSelectedAddress { get; private set; }

        private BankEquipment selectedBankEquipment;

        public BankEquipment SelectedBankEquipment
        {
            get { return selectedBankEquipment; }
            set
            {
                selectedBankEquipment = value;
                NotifyOfPropertyChange(() => selectedBankEquipment);
            }
        }

        private BindableCollection<ServiceAddress> serviceAddresses;

        public BindableCollection<ServiceAddress> ServiceAddresses
        {
            get { return serviceAddresses; }
            set
            {
                serviceAddresses = value;
                NotifyOfPropertyChange(() => ServiceAddresses);
            }
        }

        private ServiceAddress selectedServiceAddress;

        public ServiceAddress SelectedServiceAddress
        {
            get { return selectedServiceAddress; }
            set
            {
                selectedServiceAddress = value;
                BankEquipmentsFromSelectedAddress = 
                    new BindableCollection<BankEquipment>(bankEquipmentRepository.GetAllById(SelectedServiceAddress.ServiceAddressId));
                NotifyOfPropertyChange(() => SelectedServiceAddress);
                NotifyOfPropertyChange(() => BankEquipmentsFromSelectedAddress);
            }
        }

        #endregion
    }
}
