﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;

namespace TechPoint.DesktopApp.ViewModels.TechServiceJournals
{
    public class TechServiceJournalAddViewModel : CrudViewModelBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRepository<TechServiceJournal> techServiceJournalRepository;
        private readonly IRepository<BankEquipment> bankEquipmentRepository;
        private readonly IRepository<TechWorkCategory> techWorkCategoryRepository;
        private readonly IRepository<TechWorker> techWorkerRepository;

        public TechServiceJournalAddViewModel(IEventAggregator eventAggregator,
            IRepository<TechServiceJournal> techServiceJournalRepository,
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<TechWorkCategory> techWorkCategoryRepository,
            IRepository<TechWorker> techWorkerRepository)
        {
            this.eventAggregator = eventAggregator;
            this.techServiceJournalRepository = techServiceJournalRepository;
            this.bankEquipmentRepository = bankEquipmentRepository;

            NewTechServiceJournal = new TechServiceJournal();
            TechServiceJournals = new BindableCollection<TechServiceJournal>(techServiceJournalRepository.GetAll());
            BankEquipments = new BindableCollection<BankEquipment>(bankEquipmentRepository.GetAll());
            TechWorkCategories = new BindableCollection<TechWorkCategory>(techWorkCategoryRepository.GetAll());
            TechWorkers = new BindableCollection<TechWorker>(techWorkerRepository.GetAll());
        }

        public TechServiceJournalAddViewModel(IEventAggregator eventAggregator,
            IRepository<TechServiceJournal> techServiceJournalRepository,
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<TechWorkCategory> techWorkCategoryRepository,
            IRepository<TechWorker> techWorkerRepository,
            TechServiceJournal item)
        {
            this.eventAggregator = eventAggregator;
            this.techServiceJournalRepository = techServiceJournalRepository;
            this.bankEquipmentRepository = bankEquipmentRepository;

            NewTechServiceJournal = item;
            TechServiceJournals = new BindableCollection<TechServiceJournal>(techServiceJournalRepository.GetAll());
            BankEquipments = new BindableCollection<BankEquipment>(bankEquipmentRepository.GetAll());
            TechWorkCategories = new BindableCollection<TechWorkCategory>(techWorkCategoryRepository.GetAll());
            TechWorkers = new BindableCollection<TechWorker>(techWorkerRepository.GetAll());
        }

        #region Entity Properties

        private BindableCollection<TechServiceJournal> techServiceJournals;

        public BindableCollection<TechServiceJournal> TechServiceJournals
        {
            get { return techServiceJournals; }
            set
            {
                techServiceJournals = value;
                NotifyOfPropertyChange(() => TechServiceJournals);
            }
        }

        private TechServiceJournal newTechServiceJournal;

        public TechServiceJournal NewTechServiceJournal
        {
            get { return newTechServiceJournal; }
            set
            {
                newTechServiceJournal = value;
                NotifyOfPropertyChange(() => NewTechServiceJournal);
            }
        }

        private BindableCollection<BankEquipment> bankEquipments;

        public BindableCollection<BankEquipment> BankEquipments
        {
            get { return bankEquipments; }
            set
            {
                bankEquipments = value;
                NotifyOfPropertyChange(() => BankEquipments);
            }
        }

        private BankEquipment selectedBankEquipment;

        public BankEquipment SelectedBankEquipment
        {
            get { return selectedBankEquipment; }
            set
            {
                selectedBankEquipment = value;
                NotifyOfPropertyChange(() => SelectedBankEquipment);
            }
        }

        private BindableCollection<TechWorkCategory> techWorkCategories;

        public BindableCollection<TechWorkCategory> TechWorkCategories
        {
            get { return techWorkCategories; }
            set
            {
                techWorkCategories = value;
                NotifyOfPropertyChange(() => TechWorkCategories);
            }
        }

        private TechWorkCategory selectedTechWorkCategory;

        public TechWorkCategory SelectedTechWorkCategory
        {
            get { return selectedTechWorkCategory; }
            set
            {
                selectedTechWorkCategory = value;
                NotifyOfPropertyChange(() => SelectedTechWorkCategory);
            }
        }

        private BindableCollection<TechWorker> techWorkers;

        public BindableCollection<TechWorker> TechWorkers
        {
            get { return techWorkers; }
            set
            {
                techWorkers = value;
                NotifyOfPropertyChange(() => TechWorkers);
            }
        }

        private TechWorker selectedTechWorker;

        public TechWorker SelectedTechWorker
        {
            get { return selectedTechWorker; }
            set
            {
                selectedTechWorker = value;
                NotifyOfPropertyChange(() => SelectedTechWorker);
            }
        }

        #endregion

        public void Save()
        {
            if (NewTechServiceJournal != null)
            {
                if (NewTechServiceJournal.TechServiceJournalId == 0)
                {
                    UpdateValues();
                    techServiceJournalRepository.Add(NewTechServiceJournal);
                    eventAggregator.PublishOnUIThread("Save");
                }
                else if (NewTechServiceJournal.TechServiceJournalId != 0)
                {
                    UpdateValues();
                    techServiceJournalRepository.Update(NewTechServiceJournal);
                    eventAggregator.PublishOnUIThread("Save");
                }
            }
        }

        private void UpdateValues()
        {
            if (SelectedBankEquipment != null)
            {
                NewTechServiceJournal.BankEquipmentId = SelectedBankEquipment.BankEquipmentId;
            }
            if (SelectedTechWorkCategory != null)
            {
                NewTechServiceJournal.TechWorkCategoryId = SelectedTechWorkCategory.TechWorkCategoryId;
            }
            if (SelectedTechWorker != null)
            {
                NewTechServiceJournal.TechWorkerId = SelectedTechWorker.TechWorkerId;
            }
        }

        public void Cancel()
        {
            eventAggregator.PublishOnUIThread("Cancel");
        }
    }
}
