﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.TechServiceJournals
{
    public class TechServiceJournalAllViewModel : CrudViewModelBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRepository<TechServiceJournal> techServiceJournalRepository;
        private const String ROOT_DIALOG = "RootTechServiceJournalDialog";

        public TechServiceJournalAllViewModel(IEventAggregator eventAggregator, IRepository<TechServiceJournal> techServiceJournalRepository)
        {
            this.eventAggregator = eventAggregator;
            this.techServiceJournalRepository = techServiceJournalRepository;

            TechServiceJournals = new BindableCollection<TechServiceJournal>(techServiceJournalRepository.GetAll());
        }

        #region Entity Properties

        private BindableCollection<TechServiceJournal> techServiceJournals;

        public BindableCollection<TechServiceJournal> TechServiceJournals
        {
            get { return techServiceJournals; }
            set
            {
                techServiceJournals = value;
                NotifyOfPropertyChange(() => TechServiceJournals);
            }
        }

        private TechServiceJournal selectedTechServiceJournal;

        public TechServiceJournal SelectedTechServiceJournal
        {
            get { return selectedTechServiceJournal; }
            set
            {
                selectedTechServiceJournal = value;
                NotifyOfPropertyChange(() => SelectedTechServiceJournal);
            }
        }

        #endregion

        public void DoneWork()
        {
            if (SelectedTechServiceJournal.Done == false)
            {
                SelectedTechServiceJournal.Done = true;
                techServiceJournalRepository.Update(SelectedTechServiceJournal);
            }
            else
            {
                SelectedTechServiceJournal.Done = false;
                techServiceJournalRepository.Update(SelectedTechServiceJournal);
            }
        }

        public void Edit()
        {
            eventAggregator.PublishOnUIThread(SelectedTechServiceJournal);
        }

        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (SelectedTechServiceJournal != null)
            {
                try
                {
                    techServiceJournalRepository.Delete(SelectedTechServiceJournal);
                    TechServiceJournals.Remove(SelectedTechServiceJournal);
                    SelectedTechServiceJournal = null;
                    return true;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }

        public void InstallSoftware()
        {
            eventAggregator.PublishOnUIThread(new Install(SelectedTechServiceJournal, "Software"));
        }
    }
}
