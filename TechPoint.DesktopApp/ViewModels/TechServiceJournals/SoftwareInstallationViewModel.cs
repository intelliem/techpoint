﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.TechServiceJournals
{
    public class SoftwareInstallationViewModel : CrudViewModelBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRepository<BankEquipmentSoftwareInstallation> softwareInstallationRepository;
        private readonly IRepository<BankEquipmentSoftware> bankEquipmentSoftwareRepository;
        private const String ROOT_DIALOG = "RootSoftwareInstallationDialog";

        public SoftwareInstallationViewModel(IEventAggregator eventAggregator,
            IRepository<BankEquipmentSoftwareInstallation> softwareInstallationRepository,
            IRepository<BankEquipmentSoftware> bankEquipmentSoftwareRepository,
            Install softwareInstall)
        {
            this.eventAggregator = eventAggregator;
            this.softwareInstallationRepository = softwareInstallationRepository;
            this.bankEquipmentSoftwareRepository = bankEquipmentSoftwareRepository;

            SelectedTechServiceJournal = softwareInstall.Ttem as TechServiceJournal;
            SoftwareInstallations =
                new BindableCollection<BankEquipmentSoftwareInstallation>(softwareInstallationRepository.GetAllById(SelectedTechServiceJournal.TechServiceJournalId));
            BankEquipmentSoftwares = new BindableCollection<BankEquipmentSoftware>(bankEquipmentSoftwareRepository.GetAll());

            SelectedSoftwareInstallation = new BankEquipmentSoftwareInstallation();
        }

        private BindableCollection<BankEquipmentSoftware> bankEquipmentSoftwares;

        public BindableCollection<BankEquipmentSoftware> BankEquipmentSoftwares
        {
            get { return bankEquipmentSoftwares; }
            set
            {
                bankEquipmentSoftwares = value;
                NotifyOfPropertyChange(() => BankEquipmentSoftwares);
            }
        }


        private TechServiceJournal selectedTechServiceJournal;

        public TechServiceJournal SelectedTechServiceJournal
        {
            get { return selectedTechServiceJournal; }
            set
            {
                selectedTechServiceJournal = value;
                NotifyOfPropertyChange(() => SelectedTechServiceJournal);
            }
        }

        private BankEquipmentSoftwareInstallation selectedSoftwareInstallation;

        public BankEquipmentSoftwareInstallation SelectedSoftwareInstallation
        {
            get { return selectedSoftwareInstallation; }
            set
            {
                selectedSoftwareInstallation = value;
                NotifyOfPropertyChange(() => SelectedSoftwareInstallation);
            }
        }


        private BankEquipmentSoftwareInstallation newSoftwareInstallation;

        public BankEquipmentSoftwareInstallation NewSoftwareInstallation
        {
            get { return newSoftwareInstallation; }
            set
            {
                newSoftwareInstallation = value;
                NotifyOfPropertyChange(() => newSoftwareInstallation);
            }
        }


        private BindableCollection<BankEquipmentSoftwareInstallation> softwareInstallations;

        public BindableCollection<BankEquipmentSoftwareInstallation> SoftwareInstallations
        {
            get { return softwareInstallations; }
            set
            {
                softwareInstallations = value;
                NotifyOfPropertyChange(() => SoftwareInstallations);
            }
        }

        private BankEquipmentSoftware selectedBankEquipmentSoftware;

        public BankEquipmentSoftware SelectedBankEquipmentSoftware
        {
            get { return selectedBankEquipmentSoftware; }
            set
            {
                selectedBankEquipmentSoftware = value;
                NotifyOfPropertyChange(() => SelectedBankEquipmentSoftware);
            }
        }

        public void Add()
        {
            SelectedSoftwareInstallation = new BankEquipmentSoftwareInstallation();
        }

        public void Save()
        {
            if (SelectedSoftwareInstallation != null)
            {
                if (SelectedSoftwareInstallation.TechServiceJournalId == 0)
                {
                    UpdateValues();
                    softwareInstallationRepository.Add(SelectedSoftwareInstallation);
                    SoftwareInstallations = new BindableCollection<BankEquipmentSoftwareInstallation>(softwareInstallationRepository.GetAllById(SelectedTechServiceJournal.TechServiceJournalId));
                }
                else if (SelectedSoftwareInstallation.TechServiceJournalId != 0)
                {
                    UpdateValues();
                    softwareInstallationRepository.Update(SelectedSoftwareInstallation);
                    SoftwareInstallations = new BindableCollection<BankEquipmentSoftwareInstallation>(softwareInstallationRepository.GetAllById(SelectedTechServiceJournal.TechServiceJournalId));
                }
            }
        }

        private void UpdateValues()
        {
            if (SelectedBankEquipmentSoftware != null)
            {
                SelectedSoftwareInstallation.BankEquipmentSoftwareId = SelectedBankEquipmentSoftware.BankEquipmentSoftwareId;
            }
            SelectedSoftwareInstallation.TechServiceJournalId = SelectedTechServiceJournal.TechServiceJournalId;
        }

        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (SelectedSoftwareInstallation != null)
            {
                try
                {
                    softwareInstallationRepository.Delete(SelectedSoftwareInstallation);
                    SoftwareInstallations.Remove(SelectedSoftwareInstallation);
                    SelectedSoftwareInstallation = new BankEquipmentSoftwareInstallation();
                    return true;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }

    }
}
