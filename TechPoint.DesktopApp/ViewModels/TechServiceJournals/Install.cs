﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechPoint.DesktopApp.ViewModels.TechServiceJournals
{
    public class Install
    {
        public object Ttem { get; set; }
        public string Message { get; set; }

        public Install()
        {

        }

        public Install(object item, string message)
        {
            Ttem = item;
            Message = message;
        }
    }
}
