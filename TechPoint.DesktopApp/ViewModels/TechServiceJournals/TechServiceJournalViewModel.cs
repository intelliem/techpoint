﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;

namespace TechPoint.DesktopApp.ViewModels.TechServiceJournals
{
    public class TechServiceJournalViewModel : Conductor<IForm>.Collection.OneActive, ITab, IHandle<string>, IHandle<TechServiceJournal>, IHandle<Install>
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRepository<TechServiceJournal> techServiceJournalRepository;
        private readonly IRepository<BankEquipment> bankEquipmentRepository;
        private readonly IRepository<TechWorkCategory> techWorkCategoryRepository;
        private readonly IRepository<TechWorker> techWorkerRepository;
        private readonly IRepository<BankEquipmentSoftware> bankEquipmentSoftwareRepository;
        private readonly IRepository<BankEquipmentSoftwareInstallation> softwareInstallationRepository;

        public TechServiceJournalViewModel(IEventAggregator eventAggregator,
            IRepository<TechServiceJournal> techServiceJournalRepository,
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<TechWorkCategory> techWorkCategoryRepository,
            IRepository<TechWorker> techWorkerRepository,
            IRepository<BankEquipmentSoftware> bankEquipmentSoftwareRepository,
            IRepository<BankEquipmentSoftwareInstallation> softwareInstallationRepository)
        {
            this.eventAggregator = eventAggregator;
            this.techServiceJournalRepository = techServiceJournalRepository;
            this.bankEquipmentRepository = bankEquipmentRepository;
            this.techWorkCategoryRepository = techWorkCategoryRepository;
            this.techWorkerRepository = techWorkerRepository;
            this.bankEquipmentSoftwareRepository = bankEquipmentSoftwareRepository;
            this.softwareInstallationRepository = softwareInstallationRepository;

            DisplayName = "Журнал обслуживания БО";

            eventAggregator.Subscribe(this);
            ActivateItem(new TechServiceJournalAllViewModel(eventAggregator, techServiceJournalRepository));
        }

        public void TechRepairJournalAddNavigator()
        {
            ActivateItem(new TechServiceJournalAddViewModel(
                eventAggregator,
                techServiceJournalRepository,
                bankEquipmentRepository,
                techWorkCategoryRepository,
                techWorkerRepository));
        }

        public void Handle(string message)
        {
            if (message.Equals("Cancel"))
            {
                ActivateItem(new TechServiceJournalAllViewModel(eventAggregator, techServiceJournalRepository));
            }

            if (message.Equals("Save"))
            {
                ActivateItem(new TechServiceJournalAllViewModel(eventAggregator, techServiceJournalRepository));
            }
        }

        public void Handle(TechServiceJournal item)
        {
            ActivateItem(new TechServiceJournalAddViewModel(
                eventAggregator,
                techServiceJournalRepository,
                bankEquipmentRepository,
                techWorkCategoryRepository,
                techWorkerRepository,
                item));
        }

        public void Handle(Install install)
        {
            if (install.Message.Equals("Software"))
            {
                ActivateItem(new SoftwareInstallationViewModel(eventAggregator, 
                    softwareInstallationRepository,
                    bankEquipmentSoftwareRepository, 
                    install));
            }
        }
    }
}
