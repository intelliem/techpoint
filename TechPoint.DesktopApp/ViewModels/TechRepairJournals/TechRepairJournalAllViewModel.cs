﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.Infrastructure.Service;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.ViewModels.TechRepairJournals
{
    public class TechRepairJournalAllViewModel : CrudViewModelBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRepository<TechRepairJournal> techRepairJournalRepository;
        private const String ROOT_DIALOG = "RootTechRepairJournaltDialog";

        public TechRepairJournalAllViewModel(IEventAggregator eventAggregator,
             IRepository<TechRepairJournal> techRepairJournalRepository)
        {
            this.eventAggregator = eventAggregator;
            this.techRepairJournalRepository = techRepairJournalRepository;

            TechRepairJournals = new BindableCollection<TechRepairJournal>(techRepairJournalRepository.GetAll());
        }

        #region Entity Properties

        private BindableCollection<TechRepairJournal> techRepairJournals;

        public BindableCollection<TechRepairJournal> TechRepairJournals
        {
            get { return techRepairJournals; }
            set
            {
                techRepairJournals = value;
                NotifyOfPropertyChange(() => TechRepairJournals);
            }
        }

        private TechRepairJournal selectedTechRepairJournal;

        public TechRepairJournal SelectedTechRepairJournal
        {
            get { return selectedTechRepairJournal; }
            set
            {
                selectedTechRepairJournal = value;
                NotifyOfPropertyChange(() => SelectedTechRepairJournal);
            }
        }

        #endregion

        public void DoneWork()
        {
            if (SelectedTechRepairJournal.Done == false)
            {
                SelectedTechRepairJournal.Done = true;
                techRepairJournalRepository.Update(SelectedTechRepairJournal);
            }
            else
            {
                SelectedTechRepairJournal.Done = false;
                techRepairJournalRepository.Update(SelectedTechRepairJournal);
            }
        }

        public void Edit()
        {
            eventAggregator.PublishOnUIThread(SelectedTechRepairJournal);
        }

        public void Delete()
        {
            DialogService.ExecuteTask = () => ExecuteDeleteTask();
            DialogService.ShowDialog(new DeleteDialogModalView(), ROOT_DIALOG);
        }

        private Boolean ExecuteDeleteTask()
        {
            if (SelectedTechRepairJournal != null)
            {
                try
                {
                    techRepairJournalRepository.Delete(SelectedTechRepairJournal);
                    TechRepairJournals.Remove(SelectedTechRepairJournal);
                    SelectedTechRepairJournal = null;
                    return true;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
    }
}
