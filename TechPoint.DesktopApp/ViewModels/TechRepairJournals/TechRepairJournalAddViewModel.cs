﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;

namespace TechPoint.DesktopApp.ViewModels.TechRepairJournals
{
    public class TechRepairJournalAddViewModel : CrudViewModelBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRepository<TechRepairJournal> techRepairJournalRepository;
        private readonly IRepository<BankEquipment> bankEquipmentRepository;

        public TechRepairJournalAddViewModel(IEventAggregator eventAggregator,
            IRepository<TechRepairJournal> techRepairJournalRepository,
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<TechWorkCategory> techWorkCategoryRepository,
            IRepository<TechWorker> techWorkerRepository)
        {
            this.eventAggregator = eventAggregator;
            this.techRepairJournalRepository = techRepairJournalRepository;
            this.bankEquipmentRepository = bankEquipmentRepository;

            NewTechRepairJournal = new TechRepairJournal();
            TechRepairJournals = new BindableCollection<TechRepairJournal>(techRepairJournalRepository.GetAll());
            BankEquipments = new BindableCollection<BankEquipment>(bankEquipmentRepository.GetAll());
            TechWorkCategories = new BindableCollection<TechWorkCategory>(techWorkCategoryRepository.GetAll());
            TechWorkers = new BindableCollection<TechWorker>(techWorkerRepository.GetAll());
        }

        public TechRepairJournalAddViewModel(IEventAggregator eventAggregator,
            IRepository<TechRepairJournal> techRepairJournalRepository,
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<TechWorkCategory> techWorkCategoryRepository,
            IRepository<TechWorker> techWorkerRepository,
            TechRepairJournal item)
        {
            this.eventAggregator = eventAggregator;
            this.techRepairJournalRepository = techRepairJournalRepository;
            this.bankEquipmentRepository = bankEquipmentRepository;

            NewTechRepairJournal = item;
            TechRepairJournals = new BindableCollection<TechRepairJournal>(techRepairJournalRepository.GetAll());
            BankEquipments = new BindableCollection<BankEquipment>(bankEquipmentRepository.GetAll());
            TechWorkCategories = new BindableCollection<TechWorkCategory>(techWorkCategoryRepository.GetAll());
            TechWorkers = new BindableCollection<TechWorker>(techWorkerRepository.GetAll());
        }

        #region Entity Properties

        private BindableCollection<TechRepairJournal> techRepairJournals;

        public BindableCollection<TechRepairJournal> TechRepairJournals
        {
            get { return techRepairJournals; }
            set
            {
                techRepairJournals = value;
                NotifyOfPropertyChange(() => TechRepairJournals);
            }
        }

        private TechRepairJournal newTechRepairJournal;

        public TechRepairJournal NewTechRepairJournal
        {
            get { return newTechRepairJournal; }
            set
            {
                newTechRepairJournal = value;
                NotifyOfPropertyChange(() => NewTechRepairJournal);
            }
        }

        private BindableCollection<BankEquipment> bankEquipments;

        public BindableCollection<BankEquipment> BankEquipments
        {
            get { return bankEquipments; }
            set
            {
                bankEquipments = value;
                NotifyOfPropertyChange(() => BankEquipments);
            }
        }

        private BankEquipment selectedBankEquipment;

        public BankEquipment SelectedBankEquipment
        {
            get { return selectedBankEquipment; }
            set
            {
                selectedBankEquipment = value;
                NotifyOfPropertyChange(() => SelectedBankEquipment);
            }
        }

        private BindableCollection<TechWorkCategory> techWorkCategories;

        public BindableCollection<TechWorkCategory> TechWorkCategories
        {
            get { return techWorkCategories; }
            set
            {
                techWorkCategories = value;
                NotifyOfPropertyChange(() => TechWorkCategories);
            }
        }

        private TechWorkCategory selectedTechWorkCategory;

        public TechWorkCategory SelectedTechWorkCategory
        {
            get { return selectedTechWorkCategory; }
            set
            {
                selectedTechWorkCategory = value;
                NotifyOfPropertyChange(() => SelectedTechWorkCategory);
            }
        }

        private BindableCollection<TechWorker> techWorkers;

        public BindableCollection<TechWorker> TechWorkers
        {
            get { return techWorkers; }
            set
            {
                techWorkers = value;
                NotifyOfPropertyChange(() => TechWorkers);
            }
        }

        private TechWorker selectedTechWorker;

        public TechWorker SelectedTechWorker
        {
            get { return selectedTechWorker; }
            set
            {
                selectedTechWorker = value;
                NotifyOfPropertyChange(() => SelectedTechWorker);
            }
        }


        #endregion

        public void Save()
        {
            if (NewTechRepairJournal != null)
            {
                if (NewTechRepairJournal.TechRepairJournalId == 0)
                {
                    UpdateValues();
                    techRepairJournalRepository.Add(NewTechRepairJournal);
                    eventAggregator.PublishOnUIThread("Save");
                }
                else if (NewTechRepairJournal.TechRepairJournalId != 0)
                {
                    UpdateValues();
                    techRepairJournalRepository.Update(NewTechRepairJournal);
                    eventAggregator.PublishOnUIThread("Save");
                }
            }
        }

        private void UpdateValues()
        {
            if (SelectedBankEquipment != null)
            {
                NewTechRepairJournal.BankEquipmentId = SelectedBankEquipment.BankEquipmentId;
            }
            if (SelectedTechWorkCategory != null)
            {
                NewTechRepairJournal.TechWorkCategoryId = SelectedTechWorkCategory.TechWorkCategoryId;
            }
            if (SelectedTechWorker != null)
            {
                NewTechRepairJournal.TechWorkerId = SelectedTechWorker.TechWorkerId;
            }
        }

        public void Cancel()
        {
            eventAggregator.PublishOnUIThread("Cancel");
        }
    }
}
