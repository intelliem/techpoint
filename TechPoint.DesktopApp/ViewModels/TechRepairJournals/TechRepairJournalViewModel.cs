﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;
using TechPoint.DesktopApp.Infrastructure;
using TechPoint.DesktopApp.ViewModels.TechRepairJournals;

namespace TechPoint.DesktopApp.ViewModels.TechRepairJournals
{
    public class TechRepairJournalViewModel : Conductor<IForm>.Collection.OneActive, ITab, IHandle<string>, IHandle<TechRepairJournal>
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRepository<TechRepairJournal> techRepairJournalRepository;
        private readonly IRepository<BankEquipment> bankEquipmentRepository;
        private readonly IRepository<TechWorkCategory> techWorkCategoryRepository;
        private readonly IRepository<TechWorker> techWorkerRepository;

        public TechRepairJournalViewModel(IEventAggregator eventAggregator,
            IRepository<TechRepairJournal> techRepairJournalRepository,
            IRepository<BankEquipment> bankEquipmentRepository,
            IRepository<TechWorkCategory> techWorkCategoryRepository,
            IRepository<TechWorker> techWorkerRepository)
        {
            this.eventAggregator = eventAggregator;
            this.techRepairJournalRepository = techRepairJournalRepository;
            this.bankEquipmentRepository = bankEquipmentRepository;
            this.techWorkCategoryRepository = techWorkCategoryRepository;
            this.techWorkerRepository = techWorkerRepository;

            DisplayName = "Журнал ремонтных работ БО";

            eventAggregator.Subscribe(this);
            ActivateItem(new TechRepairJournalAllViewModel(eventAggregator, techRepairJournalRepository));
        }

        public void Handle(string message)
        {
            if (message.Equals("Cancel"))
            {
                ActivateItem(new TechRepairJournalAllViewModel(eventAggregator, techRepairJournalRepository));
            }

            if (message.Equals("Save"))
            {
                ActivateItem(new TechRepairJournalAllViewModel(eventAggregator, techRepairJournalRepository));
            }
        }

        public void Handle(TechRepairJournal item)
        {
            ActivateItem(new TechRepairJournalAddViewModel(
                eventAggregator,
                techRepairJournalRepository,
                bankEquipmentRepository,
                techWorkCategoryRepository,
                techWorkerRepository, 
                item));
        }

        public void TechRepairJournalAddNavigator()
        {
            ActivateItem(new TechRepairJournalAddViewModel(
                eventAggregator,
                techRepairJournalRepository,
                bankEquipmentRepository,
                techWorkCategoryRepository,
                techWorkerRepository));
        }
    }
}
