namespace TechPoint.DesktopApp
{
    using System;
    using System.Collections.Generic;
    using Caliburn.Micro;
    using TechPoint.DataService.Infrastructure;
    using TechPoint.DataService.Models;
    using TechPoint.DataService.Repositories;
    using TechPoint.DesktopApp.Infrastructure;
    using TechPoint.DesktopApp.ViewModels;
    using TechPoint.DesktopApp.ViewModels.BankEquipments;
    using TechPoint.DesktopApp.ViewModels.References;
    using TechPoint.DesktopApp.ViewModels.References.BankEquipmentAccessories;
    using TechPoint.DesktopApp.ViewModels.References.BankEquipmentSoftwares;
    using TechPoint.DesktopApp.ViewModels.References.BankEquipmentTypes;
    using TechPoint.DesktopApp.ViewModels.References.TechWorkCategories;
    using TechPoint.DesktopApp.ViewModels.References.TechWorkers;
    using TechPoint.DesktopApp.ViewModels.Reports;
    using TechPoint.DesktopApp.ViewModels.TechRepairJournals;
    using TechPoint.DesktopApp.ViewModels.TechServiceJournals;

    public class AppBootstrapper : BootstrapperBase
    {
        SimpleContainer container;


        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            container = new SimpleContainer();

            container.Instance(container);

            container.Singleton<IWindowManager, WindowManager>();
            container.Singleton<IEventAggregator, EventAggregator>();
            container.Singleton<IRepository<BankEquipment>, BankEquipmentRepository>();
            container.Singleton<IRepository<BankEquipmentType>, BankEquipmentTypeRepository>();
            container.Singleton<IRepository<Manufacturer>, ManufacturerRepository>();
            container.Singleton<IRepository<ServiceAddress>, ServiceAddressRepository>();
            container.Singleton<IRepository<BankEquipmentSoftware>, BankEquipmentSoftwareRepository>();
            container.Singleton<IRepository<SoftwareType>, SoftwareTypeRepository>();
            container.Singleton<IRepository<BankEquipmentAccessory>, BankEquipmentAccessoryRepository>();
            container.Singleton<IRepository<AccessoryType>, AccessoryTypeRepository>();
            container.Singleton<IRepository<TechWorker>, TechWorkerRepository>();
            container.Singleton<IRepository<Post>, PostRepository>();
            container.Singleton<IRepository<TechWorkCategory>, TechWorkCategoryRepository>();
            container.Singleton<IRepository<TechRepairJournal>, TechRepairJournalRepository>();
            container.Singleton<IRepository<TechServiceJournal>, TechServiceJournalRepository>();
            container.Singleton<IRepository<BankEquipmentSoftwareInstallation>, BankEquipmentSoftwareInstallationRepository>();

            container.PerRequest<IShell, MainViewModel>();
            container.PerRequest<ITab, BankEquipmentViewModel>();
            container.PerRequest<IScreen, BankEquipmentAddViewModel>();
            container.PerRequest<IScreen, BankEquipmentAllViewModel>();

            container.PerRequest<ITab, TechRepairJournalViewModel>();
            container.PerRequest<ITab, TechServiceJournalViewModel>();
            container.PerRequest<IScreen, SoftwareInstallationViewModel>();

            container.PerRequest<ITab, ReportViewModel>();

            container.PerRequest<ITab, ReferencesViewModel>();
            container.PerRequest<IReferenceTab, TechWorkerViewModel>();
            container.PerRequest<IReferenceTab, TechWorkCategoryViewModel>();
            container.PerRequest<IReferenceTab, BankEquipmentTypeViewModel>();
            container.PerRequest<IReferenceTab, BankEquipmentAccessoryViewModel>();
            container.PerRequest<IReferenceTab, BankEquipmentSoftwareViewModel>();
        }

        protected override object GetInstance(Type service, string key)
        {
            return container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }

        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {
            DisplayRootViewFor<IShell>();
        }
    }
}