﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DesktopApp.Infrastructure.Views;

namespace TechPoint.DesktopApp.Infrastructure.Service
{
    public class DialogService
    {
        public static Func<Boolean> ExecuteTask { get; set; }

        public static void ShowDialog(Object content, String rootDialog)
        {
            DialogHost.Show(content, rootDialog, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
        }

        private static void ExtendedOpenedEventHandler(object sender, DialogOpenedEventArgs eventargs)
        {
            Console.WriteLine("You could intercept the open and affect the dialog using eventArgs.Session.");
        }

        private static void ExtendedClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if ((bool)eventArgs.Parameter == false)
            {
                return;
            }
            else
            {
                //OK, lets cancel the close...
                eventArgs.Cancel();

                //...now, lets update the "session" with some new content!
                eventArgs.Session.UpdateContent(new ProgressDialogView());
                //note, you can also grab the session when the dialog opens via the DialogOpenedEventHandler

                //lets run a fake operation for 3 seconds then close this baby.
                Task.Delay(TimeSpan.FromSeconds(1))
                    .ContinueWith((t, _) =>
                    {
                        ExecuteTask.Invoke();
                        eventArgs.Session.Close(false);
                    }, null,
                        TaskScheduler.FromCurrentSynchronizationContext());
            }
        }
    }
}
