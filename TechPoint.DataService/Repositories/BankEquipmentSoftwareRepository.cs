﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class BankEquipmentSoftwareRepository : IRepository<BankEquipmentSoftware>
    {
        private readonly TechPointContext context;

        public BankEquipmentSoftwareRepository()
        {
            context = new TechPointContext();
        }

        public void Add(BankEquipmentSoftware entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            context.BankEquipmentSoftwares.Add(entity);
            context.SaveChanges();
        }

        public void Delete(BankEquipmentSoftware entity)
        {
            try
            {
                context.BankEquipmentSoftwares.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public ICollection<BankEquipmentSoftware> GetAll()
        {
            return context.BankEquipmentSoftwares.Include("SoftwareType").OrderBy(o => o.Title).ToList();
        }

        public ICollection<BankEquipmentSoftware> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public BankEquipmentSoftware GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(BankEquipmentSoftware entity)
        {
            var oldEntity = context.BankEquipmentSoftwares.Single(e => e.BankEquipmentSoftwareId == entity.BankEquipmentSoftwareId);
            oldEntity.BankEquipmentSoftwareId = entity.BankEquipmentSoftwareId;
            oldEntity.Title = entity.Title;
            oldEntity.SoftwareTypeId = entity.SoftwareTypeId;
            oldEntity.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }
    }
}
