﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class TechRepairJournalRepository : IRepository<TechRepairJournal>
    {
        private readonly TechPointContext context;

        public TechRepairJournalRepository()
        {
            context = new TechPointContext();
        }

        public void Add(TechRepairJournal entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            context.TechRepairJournals.Add(entity);
            context.SaveChanges();
        }

        public void Delete(TechRepairJournal entity)
        {
            try
            {
                context.TechRepairJournals.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public ICollection<TechRepairJournal> GetAll()
        {
            return context.TechRepairJournals
                .Include("BankEquipment")
                .Include("TechWorkCategory")
                .Include("TechWorker")
                .OrderBy(t => t.StartDate).ToList();
        }

        public ICollection<TechRepairJournal> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public TechRepairJournal GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(TechRepairJournal entity)
        {
            var oldEntity = context.TechRepairJournals.Single(e => e.TechRepairJournalId == entity.TechRepairJournalId);
            oldEntity.TechRepairJournalId = entity.TechRepairJournalId;
            oldEntity.Description = entity.Description;
            oldEntity.StartDate = entity.StartDate;
            oldEntity.StartTime = entity.StartTime;
            oldEntity.EndDate = entity.EndDate;
            oldEntity.EndTime = entity.EndTime;
            oldEntity.Done = entity.Done;
            oldEntity.BankEquipmentId = entity.BankEquipmentId;
            oldEntity.TechWorkCategoryId = entity.TechWorkCategoryId;
            oldEntity.TechWorkerId = entity.TechWorkerId;
            oldEntity.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }
    }
}
