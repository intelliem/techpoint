﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class ManufacturerRepository : IRepository<Manufacturer>
    {
        private readonly TechPointContext context;

        public ManufacturerRepository()
        {
            context = new TechPointContext();
        }

        public void Add(Manufacturer entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Manufacturer entity)
        {
            throw new NotImplementedException();
        }

        public ICollection<Manufacturer> GetAll()
        {
            return context.Manufacturers.OrderBy(m => m.Title).ToList();
        }

        public ICollection<Manufacturer> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public Manufacturer GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Manufacturer entity)
        {
            throw new NotImplementedException();
        }
    }
}
