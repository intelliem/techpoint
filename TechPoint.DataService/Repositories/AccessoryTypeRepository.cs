﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class AccessoryTypeRepository : IRepository<AccessoryType>
    {
        private readonly TechPointContext context;

        public AccessoryTypeRepository()
        {
            context = new TechPointContext();
        }

        public void Add(AccessoryType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(AccessoryType entity)
        {
            throw new NotImplementedException();
        }

        public ICollection<AccessoryType> GetAll()
        {
            return context.AccessoryTypes.OrderBy(p => p.Title).ToList();
        }

        public ICollection<AccessoryType> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public AccessoryType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(AccessoryType entity)
        {
            throw new NotImplementedException();
        }
    }
}
