﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class TechWorkCategoryRepository : IRepository<TechWorkCategory>
    {
        private readonly TechPointContext context;

        public TechWorkCategoryRepository()
        {
            context = new TechPointContext();
        }

        public void Add(TechWorkCategory entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            context.TechWorkCategories.Add(entity);
            context.SaveChanges();
        }

        public void Delete(TechWorkCategory entity)
        {
            try
            {
                context.TechWorkCategories.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public ICollection<TechWorkCategory> GetAll()
        {
            return context.TechWorkCategories.OrderBy(w => w.Title).ToList();
        }

        public ICollection<TechWorkCategory> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public TechWorkCategory GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(TechWorkCategory entity)
        {
            var oldEntity = context.TechWorkCategories.Single(e => e.TechWorkCategoryId == entity.TechWorkCategoryId);
            oldEntity.TechWorkCategoryId = entity.TechWorkCategoryId;
            oldEntity.Title = entity.Title;
            oldEntity.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }
    }
}
