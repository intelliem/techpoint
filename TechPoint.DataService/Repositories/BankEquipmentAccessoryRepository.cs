﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class BankEquipmentAccessoryRepository : IRepository<BankEquipmentAccessory>
    {
        private readonly TechPointContext context;

        public BankEquipmentAccessoryRepository()
        {
            context = new TechPointContext();
        }

        public void Add(BankEquipmentAccessory entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            context.BankEquipmentAccessories.Add(entity);
            context.SaveChanges();
        }

        public void Delete(BankEquipmentAccessory entity)
        {
            try
            {
                context.BankEquipmentAccessories.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public ICollection<BankEquipmentAccessory> GetAll()
        {
            return context.BankEquipmentAccessories.Include("AccessoryType").OrderBy(p => p.Title).ToList();
        }

        public ICollection<BankEquipmentAccessory> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public BankEquipmentAccessory GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(BankEquipmentAccessory entity)
        {
            var oldEntity = context.BankEquipmentAccessories.Single(e => e.BankEquipmentAccessoryId == entity.BankEquipmentAccessoryId);
            oldEntity.BankEquipmentAccessoryId = entity.BankEquipmentAccessoryId;
            oldEntity.Title = entity.Title;
            oldEntity.Description = entity.Description;
            oldEntity.SerialNumber = entity.SerialNumber;
            oldEntity.AccessoryTypeId = entity.AccessoryTypeId;
            oldEntity.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }
    }
}
