﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class ServiceAddressRepository : IRepository<ServiceAddress>
    {
        private readonly TechPointContext context;

        public ServiceAddressRepository()
        {
            context = new TechPointContext();
        }

        public void Add(ServiceAddress entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(ServiceAddress entity)
        {
            throw new NotImplementedException();
        }

        public ICollection<ServiceAddress> GetAll()
        {
            return context.ServiceAddresses.OrderBy(s => s.Street).ToList();
        }

        public ICollection<ServiceAddress> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public ServiceAddress GetById(int id)
        {
            return context.ServiceAddresses.Find(id);
        }

        public void Update(ServiceAddress entity)
        {
            throw new NotImplementedException();
        }
    }
}
