﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class BankEquipmentSoftwareInstallationRepository : IRepository<BankEquipmentSoftwareInstallation>
    {
        private readonly TechPointContext context;

        public BankEquipmentSoftwareInstallationRepository()
        {
            context = new TechPointContext();
        }

        public void Add(BankEquipmentSoftwareInstallation entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            context.BankEquipmentSoftwareInstallations.Add(entity);
            context.SaveChanges();
        }

        public void Delete(BankEquipmentSoftwareInstallation entity)
        {
            try
            {
                context.BankEquipmentSoftwareInstallations.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public ICollection<BankEquipmentSoftwareInstallation> GetAll()
        {
            throw new NotImplementedException();
        }

        public ICollection<BankEquipmentSoftwareInstallation> GetAllById(int id)
        {
            return context.BankEquipmentSoftwareInstallations.Include("BankEquipmentSoftware").Where(p => p.TechServiceJournalId == id).ToList();
        }

        public BankEquipmentSoftwareInstallation GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(BankEquipmentSoftwareInstallation entity)
        {
            var oldEquipment = context.BankEquipmentSoftwareInstallations.Single(e => e.BankEquipmentSoftwareInstallationId == entity.BankEquipmentSoftwareInstallationId);
            oldEquipment.BankEquipmentSoftwareInstallationId = entity.BankEquipmentSoftwareInstallationId;
            oldEquipment.BankEquipmentSoftwareId = entity.BankEquipmentSoftwareId;
            oldEquipment.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }
    }
}
