﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class TechWorkerRepository : IRepository<TechWorker>
    {
        private readonly TechPointContext context;

        public TechWorkerRepository()
        {
            context = new TechPointContext();
        }

        public void Add(TechWorker entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            context.TechWorkers.Add(entity);
            context.SaveChanges();
        }

        public void Delete(TechWorker entity)
        {
            try
            {
                context.TechWorkers.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public ICollection<TechWorker> GetAll()
        {
            return context.TechWorkers.Include("Post").OrderBy(w => w.LastName).ToList();
        }

        public ICollection<TechWorker> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public TechWorker GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(TechWorker entity)
        {
            var oldEntity = context.TechWorkers.Single(e => e.TechWorkerId == entity.TechWorkerId);
            oldEntity.TechWorkerId = entity.TechWorkerId;
            oldEntity.FirstName = entity.FirstName;
            oldEntity.MiddleName = entity.MiddleName;
            oldEntity.LastName = entity.LastName;
            oldEntity.PostId = entity.PostId;
            oldEntity.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }
    }
}
