﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class SoftwareTypeRepository : IRepository<SoftwareType>
    {
        private readonly TechPointContext context;

        public SoftwareTypeRepository()
        {
            context = new TechPointContext();
        }

        public void Add(SoftwareType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SoftwareType entity)
        {
            throw new NotImplementedException();
        }

        public ICollection<SoftwareType> GetAll()
        {
            return context.SoftwareTypes.OrderBy(c => c.Title).ToList();
        }

        public ICollection<SoftwareType> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public SoftwareType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(SoftwareType entity)
        {
            throw new NotImplementedException();
        }
    }
}
