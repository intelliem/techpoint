﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class BankEquipmentRepository : IRepository<BankEquipment>
    {
        private readonly TechPointContext context;

        public BankEquipmentRepository()
        {
            context = new TechPointContext();
        }

        public ICollection<BankEquipment> GetAll()
        {   
            return context.BankEquipments
                .Include("BankEquipmentType")
                .Include("Manufacturer")
                .Include("ServiceAddress")
                .OrderBy(e => e.Title).ToList();
        }

        public void Add(BankEquipment entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            entity.ManufactureDate = entity.ManufactureDate;
            context.BankEquipments.Add(entity);
            context.SaveChanges();
        }

        public void Update(BankEquipment entity)
        {
            var oldEquipment = context.BankEquipments.Single(e => e.BankEquipmentId == entity.BankEquipmentId);
            oldEquipment.BankEquipmentTypeId = entity.BankEquipmentTypeId;
            oldEquipment.Title = entity.Title;
            oldEquipment.Manufacturer = entity.Manufacturer;
            oldEquipment.ManufactureDate = entity.ManufactureDate;
            oldEquipment.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }

        public void Delete(BankEquipment entity)
        {
            try
            {
                context.BankEquipments.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public BankEquipment GetById(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<BankEquipment> GetAllById(int id)
        {
            return context.BankEquipments.Where(p => p.ServiceAddressId == id).ToList();
        }
    }
}
