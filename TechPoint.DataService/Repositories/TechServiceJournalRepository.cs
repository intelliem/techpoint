﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class TechServiceJournalRepository : IRepository<TechServiceJournal>
    {
        private readonly TechPointContext context;

        public TechServiceJournalRepository()
        {
            context = new TechPointContext();
        }

        public void Add(TechServiceJournal entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            context.TechServiceJournals.Add(entity);
            context.SaveChanges();
        }

        public void Delete(TechServiceJournal entity)
        {
            try
            {
                context.TechServiceJournals.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public ICollection<TechServiceJournal> GetAll()
        {
            return context.TechServiceJournals
                .Include("BankEquipment")
                .Include("TechWorkCategory")
                .Include("TechWorker")
                .OrderBy(t => t.StartDate).ToList();
        }

        public ICollection<TechServiceJournal> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public TechServiceJournal GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(TechServiceJournal entity)
        {
            var oldEntity = context.TechServiceJournals.Single(e => e.TechServiceJournalId == entity.TechServiceJournalId);
            oldEntity.TechServiceJournalId = entity.TechServiceJournalId;
            oldEntity.Description = entity.Description;
            oldEntity.StartDate = entity.StartDate;
            oldEntity.StartTime = entity.StartTime;
            oldEntity.EndDate = entity.EndDate;
            oldEntity.EndTime = entity.EndTime;
            oldEntity.Done = entity.Done;
            oldEntity.BankEquipmentId = entity.BankEquipmentId;
            oldEntity.TechWorkCategoryId = entity.TechWorkCategoryId;
            oldEntity.TechWorkerId = entity.TechWorkerId;
            oldEntity.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }
    }
}
