﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class PostRepository : IRepository<Post>
    {
        private readonly TechPointContext context;

        public PostRepository()
        {
            context = new TechPointContext();
        }

        public void Add(Post entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Post entity)
        {
            throw new NotImplementedException();
        }

        public ICollection<Post> GetAll()
        {
            return context.Posts.OrderBy(p => p.Title).ToList();
        }

        public ICollection<Post> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public Post GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Post entity)
        {
            throw new NotImplementedException();
        }
    }
}
