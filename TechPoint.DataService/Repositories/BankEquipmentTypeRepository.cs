﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Repositories
{
    public class BankEquipmentTypeRepository : IRepository<BankEquipmentType>
    {
        private readonly TechPointContext context;

        public BankEquipmentTypeRepository()
        {
            context = new TechPointContext();
        }

        public void Add(BankEquipmentType entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            context.BankEquipmentTypes.Add(entity);
            context.SaveChanges();
        }

        public void Delete(BankEquipmentType entity)
        {
            try
            {
                context.BankEquipmentTypes.Remove(entity);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                ContextUtil.RollBackDbChanges(context);
                ex.GetBaseException();
            }
        }

        public ICollection<BankEquipmentType> GetAll()
        {
            return context.BankEquipmentTypes.OrderBy(e => e.Title).ToList();
        }

        public ICollection<BankEquipmentType> GetAllById(int id)
        {
            throw new NotImplementedException();
        }

        public BankEquipmentType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(BankEquipmentType entity)
        {
            var oldEquipment = context.BankEquipmentTypes.Single(e => e.BankEquipmentTypeId == entity.BankEquipmentTypeId);
            oldEquipment.BankEquipmentTypeId = entity.BankEquipmentTypeId;
            oldEquipment.Title = entity.Title;
            oldEquipment.UpdateDate = DateTime.Now;
            context.SaveChanges();
        }
    }
}
