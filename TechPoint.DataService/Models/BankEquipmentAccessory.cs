﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class BankEquipmentAccessory : BaseModel
    {
        public BankEquipmentAccessory()
        {
            BankEquipmentInstallations = new List<BankEquipmentAccessoryInstallation>();
        }

        public int BankEquipmentAccessoryId { get; set; }
        public String Title { get; set; }
        public String SerialNumber { get; set; }
        public String Description { get; set; }

        public int AccessoryTypeId { get; set; }

        public virtual AccessoryType AccessoryType { get; set; }

        public virtual ICollection<BankEquipmentAccessoryInstallation> BankEquipmentInstallations { get; set; }
    }
}
