﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Adapter;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class BankEquipment : BaseModel
    {
        public BankEquipment()
        {
            TechServiceJournals = new List<TechServiceJournal>();
            TechRepairJournals = new List<TechRepairJournal>();
        }

        [Key]
        public int BankEquipmentId { get; set; }
        public String Title { get; set; }
        public String SerialNumber { get; set; }
        public String Description { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ManufactureDate { get; set; }

        public int BankEquipmentTypeId { get; set; }
        public int ManufacturerId { get; set; }
        public int ServiceAddressId { get; set; }

        public virtual BankEquipmentType BankEquipmentType { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual ServiceAddress ServiceAddress { get; set; }

        public virtual ICollection<TechServiceJournal> TechServiceJournals { get; set; }
        public virtual ICollection<TechRepairJournal> TechRepairJournals { get; set; }
    }
}
