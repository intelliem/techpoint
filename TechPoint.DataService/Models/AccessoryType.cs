﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class AccessoryType : BaseModel
    {
        public AccessoryType()
        {
            BankEquipmentAccessories = new List<BankEquipmentAccessory>();
        }

        public int AccessoryTypeId { get; set; }
        public string Title { get; set; }

        public virtual ICollection<BankEquipmentAccessory> BankEquipmentAccessories { get; set; }
    }
}
