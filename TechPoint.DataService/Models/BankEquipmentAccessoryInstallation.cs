﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class BankEquipmentAccessoryInstallation : BaseModel
    {
        [Key]
        public int BankEquipmentAccessoryInstallationId { get; set; }
        public int BankEquipmentAccessoryId { get; set; }

        public int TechServiceJournalId { get; set; }

        public virtual BankEquipmentAccessory BankEquipmentAccessory { get; set; }
    }
}
