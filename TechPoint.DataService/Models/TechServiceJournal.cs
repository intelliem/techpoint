﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class TechServiceJournal : Journal
    {
        public TechServiceJournal()
        {
            BankEquipmentAccessoryInstallations = new List<BankEquipmentAccessoryInstallation>();
            BankEquipmentSoftwareInstallations = new List<BankEquipmentSoftwareInstallation>();
        }

        public int TechServiceJournalId { get; set; }

        public int BankEquipmentId { get; set; }
        public int TechWorkCategoryId { get; set; }
        public int TechWorkerId { get; set; }

        public virtual BankEquipment BankEquipment { get; set; }
        public virtual TechWorkCategory TechWorkCategory { get; set; }
        public virtual TechWorker TechWorker { get; set; }

        public virtual ICollection<BankEquipmentAccessoryInstallation> BankEquipmentAccessoryInstallations { get; set; }
        public virtual ICollection<BankEquipmentSoftwareInstallation> BankEquipmentSoftwareInstallations { get; set; }
    }
}
