﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class ServiceAddress : BaseModel
    {
        public ServiceAddress()
        {
            BankEquipments = new List<BankEquipment>();
        }

        [Key]
        public int ServiceAddressId { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string House { get; set; }

        public ICollection<BankEquipment> BankEquipments { get; set; }
    }
}
