﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class Post : BaseModel
    {
        public Post()
        {
            TechWorkers = new List<TechWorker>();
        }

        public int PostId { get; set; }
        public String Title { get; set; }

        public virtual ICollection<TechWorker> TechWorkers { get; set; }
    }
}
