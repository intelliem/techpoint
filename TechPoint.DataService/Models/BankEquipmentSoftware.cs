﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class BankEquipmentSoftware : BaseModel
    {
        public BankEquipmentSoftware()
        {
            BankEquipmentInstallations = new List<BankEquipmentAccessoryInstallation>();
        }

        public int BankEquipmentSoftwareId { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }

        public int SoftwareTypeId { get; set; }

        public virtual SoftwareType SoftwareType { get; set; }

        public virtual ICollection<BankEquipmentAccessoryInstallation> BankEquipmentInstallations { get; set; }
    }
}
