﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class TechWorker : BaseModel
    {
        public TechWorker()
        {
            TechRepairJournals = new List<TechRepairJournal>();
            TechServiceJournals = new List<TechServiceJournal>();
        }

        public int TechWorkerId { get; set; }
        public String FirstName { get; set; }
        public String MiddleName { get; set; }
        public String LastName { get; set; }

        public int PostId { get; set; }

        public virtual Post Post { get; set; }

        public virtual ICollection<TechRepairJournal> TechRepairJournals { get; set; }
        public virtual ICollection<TechServiceJournal> TechServiceJournals { get; set; }
    }
}
