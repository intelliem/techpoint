﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class BankEquipmentSoftwareInstallation : BaseModel
    {
        [Key]
        public int BankEquipmentSoftwareInstallationId { get; set; }
        public int BankEquipmentSoftwareId { get; set; }

        public int TechServiceJournalId { get; set; }

        public virtual BankEquipmentSoftware BankEquipmentSoftware { get; set; } 
    }
}
