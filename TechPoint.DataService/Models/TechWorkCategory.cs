﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class TechWorkCategory : BaseModel
    {
        public TechWorkCategory()
        {
            TechServiceJournals = new List<TechServiceJournal>();
            TechRepairJournals = new List<TechRepairJournal>();
        }

        [Key]
        public int TechWorkCategoryId { get; set; }
        public string Title { get; set; }

        public virtual ICollection<TechServiceJournal> TechServiceJournals { get; set; }
        public virtual ICollection<TechRepairJournal> TechRepairJournals { get; set; }
    }
}
