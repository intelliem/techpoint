﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class Manufacturer : BaseModel
    {
        public Manufacturer()
        {
            BankEquipments = new List<BankEquipment>();
        }

        [Key]
        public int ManufacturerId { get; set; }
        public string Title { get; set; }

        public virtual ICollection<BankEquipment> BankEquipments { get; set; }
    }
}
