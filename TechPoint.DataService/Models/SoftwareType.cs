﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class SoftwareType : BaseModel
    {
        public SoftwareType()
        {
            BankEquipmentSoftwares = new List<BankEquipmentSoftware>();
        }

        public int SoftwareTypeId { get; set; }
        public string Title { get; set; }

        public virtual ICollection<BankEquipmentSoftware> BankEquipmentSoftwares { get; set; }
    }
}
