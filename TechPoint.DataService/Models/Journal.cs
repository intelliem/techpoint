﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class Journal : BaseModel
    {
        public String Description { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime StartDate { get; set; }
        [Column(TypeName = "time")]
        public TimeSpan StartTime { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime EndDate { get; set; }
        [Column(TypeName = "time")]
        public TimeSpan EndTime { get; set; }
        public Boolean Done { get; set; }
    }
}
