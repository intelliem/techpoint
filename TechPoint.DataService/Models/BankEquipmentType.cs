﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Infrastructure;

namespace TechPoint.DataService.Models
{
    public class BankEquipmentType : BaseModel
    {
        public BankEquipmentType()
        {
            BankEquipments = new List<BankEquipment>();
        }

        [Key]
        public int BankEquipmentTypeId { get; set; }
        public string Title { get; set; }

        public virtual ICollection<BankEquipment> BankEquipments { get; set; }
    }
}
