﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechPoint.DataService.Infrastructure
{
    public interface IRepository<T>
    {
        void Add(T entity);
        void Delete(T entity);
        ICollection<T> GetAll();
        void Update(T entity);
        T GetById(int id);
        ICollection<T> GetAllById(int id);
    }
}
