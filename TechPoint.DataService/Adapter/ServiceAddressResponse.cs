﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Models;

namespace TechPoint.DataService.Adapter
{
    public class ServiceAddressResponse
    {
        [Key]
        public int ServiceAddressId { get; set; }
        public String Address { get; set; }

        public ServiceAddressResponse(int serviceAddressId, String address)
        {
            ServiceAddressId = serviceAddressId;
            Address = address;
        }

        public ServiceAddressResponse(ServiceAddress serviceAddress)
        {
            ServiceAddressId = serviceAddress.ServiceAddressId;
            string address = serviceAddress.City + ", " + serviceAddress.Street + ", " + serviceAddress.House;
            Address = address;
        }
    }
}
