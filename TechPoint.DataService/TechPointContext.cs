﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechPoint.DataService.Migrations;
using TechPoint.DataService.Models;

namespace TechPoint.DataService
{
    class TechPointContext : DbContext
    {
        public TechPointContext() : base("TechPointContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<TechPointContext, Configuration>());
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
        }

        public virtual DbSet<BankEquipment> BankEquipments { get; set; }
        public virtual DbSet<BankEquipmentAccessory> BankEquipmentAccessories { get; set; }
        public virtual DbSet<BankEquipmentSoftware> BankEquipmentSoftwares { get; set; }
        public virtual DbSet<BankEquipmentType> BankEquipmentTypes { get; set; }

        public virtual DbSet<AccessoryType> AccessoryTypes { get; set; }
        public virtual DbSet<SoftwareType> SoftwareTypes { get; set; }

        public virtual DbSet<ServiceAddress> ServiceAddresses { get; set; }

        public virtual DbSet<TechServiceJournal> TechServiceJournals { get; set; }
        public virtual DbSet<TechRepairJournal> TechRepairJournals { get; set; }
        public virtual DbSet<TechWorkCategory> TechWorkCategories { get; set; }
        public virtual DbSet<BankEquipmentAccessoryInstallation> BankEquipmentAccessoryInstallations { get; set; }
        public virtual DbSet<BankEquipmentSoftwareInstallation> BankEquipmentSoftwareInstallations { get; set; }

        public virtual DbSet<Manufacturer> Manufacturers { get; set; }

        public virtual DbSet<TechWorker> TechWorkers { get; set; }
        public virtual DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
